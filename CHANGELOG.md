# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [6.7.0] - 2023/06/09
### Fixed
 - Rendition value when width or height are not available

## [6.6.0] - 2022/03/14
### Added
 - Release version
